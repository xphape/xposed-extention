package com.virjar.xposed_extention;

import android.annotation.SuppressLint;
import android.app.Application;
import android.util.Log;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;

/**
 * Created by virjar on 2017/12/23.
 * <br>处理XposedHelpers一些不能处理的问题
 */

public class XposedReflectUtil {

    //能够子子类向父类寻找method
    public static void findAndHookMethodWithSupperClass(Class<?> clazz, String methodName, Object... parameterTypesAndCallback) {
//        Object param[] = new Object[parameterTypesAndCallback.length - 1];
//        System.arraycopy(parameterTypesAndCallback, 0, param, 0, param.length);
//        //TODO 这里有bug，参数传递失败
//        //java.lang.NoSuchMethodError: com.tencent.livemaster.business.home.ui.HomeActivity#onCreate(java.lang.Class)#bestmatch Boundle.class变为java.lang.Class
//        Method methodBestMatch = XposedHelpers.findMethodBestMatch(clazz, methodName, param);
//        XposedBridge.hookMethod(methodBestMatch, (XC_MethodHook) parameterTypesAndCallback[parameterTypesAndCallback.length - 1]);
        Class theClazz = clazz;

        do {
            try {
                XposedHelpers.findAndHookMethod(theClazz, methodName, parameterTypesAndCallback);
                return;
            } catch (NoSuchMethodError e) {
                //ignore
            }
        } while ((theClazz = theClazz.getSuperclass()) != null);
        throw new NoSuchMethodError("no method " + methodName + " for class:" + clazz.getName());
    }


    /**
     * 很多时候只有一个名字，各种寻找参数类型太麻烦了,自带到find supper class的工作，请注意XposedBridge#hookAllMethods(java.lang.Class, java.lang.String, XC_MethodHook)并不会call supper
     *
     * @param clazz        class对象
     * @param methodName   想要被hook的方法名字
     * @param xcMethodHook 回调函数
     * @see XposedBridge#hookAllMethods(java.lang.Class, java.lang.String, XC_MethodHook)
     */
    public static void findAndHookOneMethod(Class<?> clazz, String methodName, XC_MethodHook xcMethodHook) {
        Class theClazz = clazz;

        do {
            Method[] declaredMethods = theClazz.getDeclaredMethods();
            for (Method method : declaredMethods) {
                if (method.getName().equals(methodName) && !Modifier.isAbstract(method.getModifiers())) {
                    XposedBridge.hookMethod(method, xcMethodHook);
                    return;
                }
            }
        } while ((theClazz = theClazz.getSuperclass()) != null);
        throw new NoSuchMethodError("no method " + methodName + " for class:" + clazz.getName());
    }


    public static void printAllMethod(Class clazz) {
        while (clazz != null) {
            Method[] declaredMethods = clazz.getDeclaredMethods();
            for (Method method : declaredMethods) {
                XposedBridge.log("printMethod: " + method);
                Log.i("weijia", "printMethod: " + method);
            }
            clazz = clazz.getSuperclass();
        }
    }

    /**
     * 基于堆栈回溯方法，可以找方法名字，但是存在方法重载的时候，一个methodName可能对应多个实现。此时由于行号信息不应正常，这会导致无法确定具体那个方法被调用。
     * <br>
     * 通过此方法打印所有同名函数，帮组确定那个方法被调用
     *
     * @param clazz class
     * @param name  希望被监控的方法名称
     */
    public static void monitorMethodCall(Class clazz, String name) {
        Method[] declaredMethods = clazz.getDeclaredMethods();
        for (Method method : declaredMethods) {
            if (!Modifier.isAbstract(method.getModifiers()) && method.getName().equals(name)) {
                XposedBridge.hookMethod(method, methodCallPrintHook);
            }
        }
    }

    private static SingletonXC_MethodHook methodCallPrintHook = new SingletonXC_MethodHook() {
        @Override
        protected void beforeHookedMethod(XC_MethodHook.MethodHookParam param) throws Throwable {
            Log.i("methodCall", "the method: " + param.method);
        }
    };

    @SuppressLint("PrivateApi")
    public static Application getApplicationUsingReflection() throws Exception {
        return (Application) Class.forName("android.app.ActivityThread").getMethod("currentApplication").invoke(null);
    }


}
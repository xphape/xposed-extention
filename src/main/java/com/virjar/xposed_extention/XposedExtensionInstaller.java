package com.virjar.xposed_extention;

public class XposedExtensionInstaller {
    public static void initComponent() {
        Ones.hookOnes(XposedExtensionInstaller.class, "init", new Ones.DoOnce() {
            @Override
            public void doOne(Class<?> clazz) {
                ClassLoadMonitor.setUp();
                LifeCycleFire.init();
            }
        });
    }
}
